<?php

namespace Controllers;

use Phalcon\Mvc\Controller;

use Phalcon\Db;
use Phalcon\Db\Exception;
use Phalcon\Db\Adapter\Pdo\Mysql as MysqlConnection;

class IndexController extends Controller
{
    public function index()
    {
        $db = $this->di->get('db');
        $result = $db->query(
            "show databases"
        );
        $result->setFetchMode(Db::FETCH_NUM);
        
        while ($robot = $result->fetch()) {
            print_r($robot);
        }
    }
}
