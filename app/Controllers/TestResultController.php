<?php

namespace Skiller\Controllers;

use Phalcon\Http\Response;
use Skiller\Models\QuestionAnswer;
use Skiller\Models\Test;
use Skiller\Models\TestResult;

class TestResultController extends \Phalcon\Mvc\Controller
{

    public function submit() : Response
    {
        $data = $this->request->getJsonRawBody(true);

        $errors = [];
        $answers = $data['answers'] ?? null;
        $test_id = $data['test_id'] ?? null;

        if($answers === null) {
            $errors['answers'] = 'answers is null';
        } else if(!\is_array($answers)) {
            $errors['answers'] = 'answers is not array';
        }

        if($test_id === null) {
            $errors['test_id'] = 'test_id is null';
        } else if (!\is_int($test_id)) {
            $errors['test_id'] = 'test_id is not int';
        } else {
            $test = Test::findFirst($test_id);
    
            if($test === false) {
                $errors['test_id'] = 'test with this test_id does not exist';
            }
        }        

        if(!empty($errors)) {
            return new Response(json_encode($errors), 400);
        }

        if($test->isAnswerable()) {
            $questions = $test->getQuestions();
            
            if(count($questions) === count($answers)) {
                $questions_arr = [];
    
                foreach($questions as $question) {
                    $variants_arr = array_map(function($variant) {
                        return $variant->getId();
                    }, $question->getVariants());
                    $questions_arr[$question->getId()] = $variants_arr;
                }
    
                foreach($answers as $key => $answer) {
                    $question_id = $answer['question_id'] ?? null;
                    $variant_id = $answer['variant_id'] ?? null;
    
                    if($question_id === null || $variant_id === null) {
                        $errors["answer_{$key}"] = "answer_{$key} does not have one of those properties: question_id, variant_id";
                        continue;
                    }

                    $question_obj = $questions_arr[$question_id] ?? null;
    
                    if($question_obj === null) {
                        $errors['answers'] = "question with id {$question_id} does not belong to this test";
                    } else if(in_array($variant_id, $question_obj) === false) {
                        $errors['answers'] = "answer variant with id {$variant_id} does not belong to question with id {$question_id}";
                    }    
                }
            } else {
                $errors['answers'] = 'answers count != questions count';
            }
        } else {
            $errors['test_id'] = 'this test is not ready';
        }      

        if(!empty($errors)) {
            return new Response(json_encode($errors), 400);
        }

        $test_result = new TestResult();
        $test_result->setTestId($test_id);
        $test_result->save();

        $question_answers = [];

        foreach($answers as $answer) {
            $question_answer = new QuestionAnswer();

            $question_answer->setQuestionId($answer['question_id']);
            $question_answer->setQuestionAnswerVariantId($answer['variant_id']);
            $question_answer->setTestResultId($test_result->getId());

            $question_answer->save();

            $question_answers[] = $question_answer;
        }

        $response_data = [
            'status' => 'OK',
            'data' => null
        ];

        return new Response(json_encode($response_data), 200);
    }

    public function get() : Response
    {
        $page = $this->request->getQuery('p', 'int', 1);
        if($page < 1) {
            $page = 1;
        }

        $limit = 10;
        $offset = $limit * ($page - 1);

        $result = TestResult::find(['limit' => $limit, 'offset' => $offset]);
        $data = [];

        foreach($result as $result_item) {
            $data[] = $result_item->getArrayData();
        }

        $response_data = [
            'status' => 'OK',
            'data' => $data
        ];

        return new Response(json_encode($response_data), 200);
    }

    public function delete() : Response
    {
        $data = $this->request->getJsonRawBody(true);

        $errors = [];
        $test_result_id = $data['id'] ?? null;

        if($test_result_id === null) {
            $errors['test_result_id'] = "test_result_id is null";
        } else {
            if(!\is_int($test_result_id)) {
                $errors['test_result_id'] = "value is not an integer";
            }
        }
        
        $test_result = TestResult::findFirst($test_result_id);
        
        if($test_result === false) {
            $errors['test_result_id'] = "test result with id {$test_result_id} is not found";
        }
        
        if(!empty($errors)) {
            return new Response(json_encode($errors), 400);
        }

        $test_result->delete();
        
        $response_data = [
            'status' => 'OK'
        ];
        
        return new Response(json_encode($response_data), 200);
    }

}

