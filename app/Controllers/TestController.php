<?php

namespace Skiller\Controllers;

use Phalcon\Http\Response;
use Phalcon\Http\Request;
use Skiller\Exceptions\TooManyQuestionsException;
use Skiller\Exceptions\TooManyVariantsException;
use Skiller\Models\CorrectAnswerVariant;
use Skiller\Models\Question;
use Skiller\Models\QuestionAnswerVariant;
use Skiller\Models\Test;

class TestController extends \Phalcon\Mvc\Controller
{

    public function index() : Response
    {
        /* $test = new Test();
        $test->setName("test name");        

        for ($i=0; $i < 15; $i++) { 
            $question = new Question();
            $question->setText("question {$i} task");
            $question->setStatus(Question::unanswered);
            try {
                $test->addQuestion($question);
            } catch(TooManyQuestionsException $e) {
                //echo $e->getMessage() . "\n";
            }
        }

        $test->save();
 */
        return new Response('OK');
    }

    public function foo() : Response
    {
        //$this->request->getQuery()
        $data = $this->request->getJsonRawBody(true);

        /* 
        ob_start();
        var_dump($data);
        $string = ob_get_contents();
        ob_clean(); */

        //print_r($data);
        //$query = $this->request->getQuery('val');
        //return new Response($query, 200);
        return new Response(json_encode($data), 200);
        //return new Response($string, 200);
    }

    public function create() : Response
    {
        $data = $this->request->getJsonRawBody(true);
        //return new Response('a');
        $errors = [];
        $test_name = $data['name'] ?? null;
        $questions_array = $data['questions'] ?? [];

        //$answer = $data['answer'] ?? null;

        /* if($answer !== null && !\is_int($answer)) {
            $errors['answer'] = "answer is not int";
        } */

        foreach ($questions_array as $key => $question_item) {
            $item_text = $question_item['text'] ?? null;
            $item_variants = $question_item['variants'] ?? [];
            $item_answer = $question_item['answer'] ?? null;

            if ($item_text === null || !\is_string($item_text)) {
                $errors["questions_array_{$key}"] = "item with key {$key} has invalid 'text' property";
            }

            if($item_answer === null || !\is_int($item_answer)) {
                $errors["questions_array_{$key}"] = "item with key {$key} has invalid 'answer' property";
            } else {
                if(!\is_array($item_variants)) {
                    $errors["questions_array_{$key}"] = "item with key {$key} has invalid 'variant' property ('variants' is not array)";
                } else {
                    if(\is_int($item_answer)) {
                        if($item_answer >= count($item_variants) || $item_answer < 0) {
                            $errors["questions_array_{$key}"] = "correct answer index is out of range";
                        }
                    }
                    foreach($item_variants as $item_variant) {
                        $variant_answer = $item_variant['answer'] ?? null;
                        if($variant_answer === null || !\is_string($variant_answer)) {
                            $errors["questions_array_{$key}"] = "item with key {$key} has invalid 'variant' property (variant 'answer' is invalid)";
                        }
                    }
                }
            }

        }

        if($test_name === null) {
            $errors['test_name'] = "test name is null";
        } else {
            if(!\is_string($test_name)) {
                $errors['test_name'] = "value is not a string";
            } else if(strlen($test_name) > 255) {
                $errors['test_name'] = "string length is greater than 255";
            }
        }

        if(!empty($errors)) {
            return new Response(json_encode($errors), 400);
        }

        $test = new Test();
        $test->setName($test_name);

        $variants = [];

        try {
            foreach($questions_array as $question_item) {
                $question = new Question();
                $question->setText($question_item['text']);
                $question->setStatus(Question::unanswered);
                try {
                    foreach($question_item['variants'] as $variant_item) {
                        $variant = new QuestionAnswerVariant();
                        $variant->setAnswer($variant_item['answer']);
                        $variant->setQuestionId($question->getId());
                        $variants[] = $variant;
                        $question->addVariant($variant);
                    }

                    $answer = $question_item['answer'];
                    
                    $question->setCorrectAnswer($variants[$answer]);
                    
                } catch(TooManyVariantsException $e) {
                    $errors['questions_array'] = $e->getMessage();
                }
                $test->addQuestion($question);
            }
        } catch(TooManyQuestionsException $e) {
            $errors['questions_array'] = $e->getMessage();
        }

        if(!empty($errors)) {
            return new Response(json_encode($errors), 400);
        }
        
        $test->save();        

        $response_data = [
            'status' => 'OK',
            'data' => $test->getArrayData()
        ];

        return new Response(json_encode($response_data));
    }

    public function get() : Response
    {
        $page = $this->request->getQuery('p', 'int', 1);
        if($page < 1) {
            $page = 1;
        }

        $limit = 10;
        $offset = $limit * ($page - 1);

        $result = Test::find(['limit' => $limit, 'offset' => $offset]);
        $data = [];

        foreach($result as $result_item) {
            $data[] = $result_item->getArrayData();
        }

        $response_data = [
            'status' => 'OK',
            'data' => $data
        ];

        return new Response(json_encode($response_data), 200);
    }

    public function delete() : Response
    {
        $data = $this->request->getJsonRawBody(true);

        $errors = [];
        $test_id = $data['id'] ?? null;

        if($test_id === null) {
            $errors['test_id'] = "test_id is null";
        } else {
            if(!\is_int($test_id)) {
                $errors['test_id'] = "value is not an integer";
            }
        }
        
        $test = Test::findFirst($test_id);
        
        if($test === false) {
            $errors['test'] = "test with id {$test_id} is not found";
        }
        
        if(!empty($errors)) {
            return new Response(json_encode($errors), 400);
        }
       /*  ob_start();
        var_dump($test_id);
        //var_dump($test == false);
        //var_dump(empty($test));
        var_dump($test);
        //echo get_class($test);
        $string = ob_get_contents();
        ob_clean(); */

        $test->delete();
        
        $response_data = [
            'status' => 'OK'
        ];
        
        return new Response(json_encode($response_data), 200);
        //$this->filter->sa

        //$errors = [];
        //$test_name = $data['name'] ?? null;
    }

}
