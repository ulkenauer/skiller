<?php

use Phinx\Migration\AbstractMigration;

class CorrectAnswerVariantKeys extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('correct_answer_variant');
        $table->addIndex('question_id', ['unique' => true]);
        $table
            ->addForeignKey('question_id', 'question', 'id', ['delete' => 'CASCADE', 'update' => 'CASCADE'])
            ->addForeignKey('question_answer_variant_id', 'question_answer_variant', 'id', ['delete' => 'CASCADE', 'update' => 'CASCADE']);
        $table->update();
    }
}
