<?php

use Phinx\Migration\AbstractMigration;

class Question extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('question', ['id' => false, 'primary_key' => 'id', 'collation' => 'utf8mb4_general_ci']);
        $table
            ->addColumn('id', 'biginteger', ['identity' => true, 'signed' => false])
            ->addColumn('test_id', 'biginteger', ['signed' => false])
            ->addColumn('status', 'integer', ['limit' => 1, 'signed' => false])
            ->addColumn('text', 'string', ['collation' => 'utf8mb4_general_ci', 'encoding' => 'utf8mb4']);

        $table->create();
    }
}
