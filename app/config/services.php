<?php

use Phalcon\Di\FactoryDefault;
$di = new FactoryDefault();

$config = include __DIR__ . '/config.php';

$di->setShared('db', function () use ($config) {

    $dbConfig = $config->database->toArray();

    $adapter = $dbConfig['adapter'];
    unset($dbConfig['adapter']);

    $class = '\Phalcon\Db\Adapter\Pdo\\' . $adapter;

    $dbConfig['options']  = [
        PDO::ATTR_EMULATE_PREPARES   => false,
        PDO::ATTR_STRINGIFY_FETCHES  => false,
    ];

    return new $class($dbConfig);
});

return $di;