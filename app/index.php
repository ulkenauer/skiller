<?php
//echo "asdas";

/* echo "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}<br>";
echo "{$_PATH_INFO}<br>";
echo "sad<br>";
print(json_encode($_SERVER));
 */

/* 
use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Application;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Url as UrlProvider;

// Define some absolute path constants to aid in locating resources
define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

// Register an autoloader
$loader = new Loader();

$loader->registerDirs(
    [
        APP_PATH . '/controllers/',
        APP_PATH . '/models/',
    ]
);

$loader->register();

// Create a DI
$di = new FactoryDefault();

// Setup the view component
$di->set(
    'view',
    function () {
        $view = new View();
        $view->setViewsDir(APP_PATH . '/views/');
        return $view;
    }
);

// Setup a base URI
$di->set(
    'url',
    function () {
        $url = new UrlProvider();
        $url->setBaseUri('/');
        return $url;
    }
);

$application = new Application($di);

try {
    // Handle the request
    $response = $application->handle();

    $response->send();
} catch (\Exception $e) {
    echo 'Exception: ', $e->getMessage();
} */

use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Router;
use Phalcon\Mvc\Micro\Collection as MicroCollection;
use Phalcon\Di\FactoryDefault;

//$di = new FactoryDefault();

require_once __DIR__ . '/app.php';

/* 
use \GuzzleHttp\Client;

$client = new Client();
$response = $client->get('google.com');
echo (string) $response->getBody(); */

//use \Skiller\Controllers\IndexController;


$app = new Micro($di);

/* $Index = new MicroCollection();
$Index->setHandler(IndexController::class, true);
$Index->get('/', 'index');
 */

$Test = new MicroCollection();
$Test->setHandler(\Skiller\Controllers\TestController::class, true);
$Test->setPrefix('/test');
$Test->post('/', 'foo');
$Test->get('/', 'index');
$Test->get('/get', 'get');
$Test->post('/create', 'create');
$Test->delete('/delete', 'delete');

$app->mount($Test);

$TestResult = new MicroCollection();
$TestResult->setHandler(\Skiller\Controllers\TestResultController::class, true);
$TestResult->setPrefix('/test_result');
$TestResult->get('/get', 'get');
$TestResult->post('/submit', 'submit');
$TestResult->delete('/delete', 'delete');

$app->mount($TestResult);

try{
    $app->handle();
} catch (Exception $e) {
    $app->response->setStatusCode(500, 'Internal Server Error');
    $app->response->setJsonContent([$e->getMessage(), $e->getTraceAsString()]);
    $app->response->setContentType('application/json');
    $app->response->send();
}
