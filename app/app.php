<?php

error_reporting(E_ALL);
date_default_timezone_set('UTC');

$loader = new \Phalcon\Loader();

$loader->registerNamespaces([
    'Skiller' => __DIR__
]);

$loader->register();

require_once __DIR__ . '/vendor/autoload.php';

$di = include __DIR__ . '/config/services.php';